FROM debian:sid

COPY group passwd shadow /etc/

RUN apt-get update \
 && apt-get install -y --no-install-recommends \
    libc-bin \
    libwayland-cursor0 \
    libwayland-egl1 \
    libfuse2 \
    libinput10 \
    libpixman-1-0 \
    libxcb-composite0 \
    libxcb-render0 \
    libxkbcommon0 \
    libegl1 \
    libgles2 \
    libgl1-mesa-dri \
    libcap2-bin \
    nix \
    git \
    ca-certificates \
    weston \
    sudo \
    firefox \
 && rm -rf /var/lib/apt/lists/* \
 && git clone --depth=1 https://github.com/L-as/waymonad.git

WORKDIR /waymonad

RUN nix-build

# agetty can only be run as root, also allows us to set capabilities
RUN chmod +s /sbin/agetty

# Temporary, can be removed when mounting home directory
RUN install -d -o will -g users /home/will

USER will
ENV XDG_RUNTIME_DIR=/tmp

ENTRYPOINT exec agetty --login-options "--caps=cap_sys_admin,cap_setpcap,cap_setuid,cap_setgid+ep --inh=cap_sys_admin --keep=1 --user=$(id -un) --addamb=cap_sys_admin -- -c $0 $@" --autologin "$(id -un)" --login-program /sbin/capsh --noclear tty$XDG_VTNR

CMD ["/waymonad/dist-newstyle/build/x86_64-linux/ghc-8.10.7/waymonad-0.0.1.0/x/waymonad/build/waymonad/waymonad"]

