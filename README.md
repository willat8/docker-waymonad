Run this in a virtual terminal with

    docker run --rm \
               --env XDG_VTNR=$XDG_VTNR \
               --cap-add=SYS_ADMIN \
               --device $(tty) \
               --device /dev/dri \
               --device /dev/input \
               --volume=/run/udev/data:/run/udev/data:ro \
               --shm-size=2G \
               --env MOZ_WEBRENDER=1 \
      willat8/waymonad:20211111

